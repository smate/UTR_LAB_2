import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Simulates an automata defined by user input.
 * User input is desribed thoroughly in the file which contains instructions for this
 * particular assignment.
 * Contains inner class Triple and Tuple which are used to describe transitions.
 * @author Simun Matesic
 *
 */
public class SimPa {
	private String currentState;
	/**
	 * A list describing particular inputs separated by a colon.
	 */
	private List<String> listOfInputs;
	/**
	 * A set of all states the automata recognizes.
	 */
	private Set<String> allStates;
	/**
	 * A set of all input symbols the automata recognizes.
	 */
	private Set<String> allSymbols;
	/**
	 * A set of all stack symbols the automata recognizes.
	 */
	private Set<String> allStackSymbols;
	/**
	 * A set of states which are considered acceptable by the automata.
	 */
	private Set<String> acceptedStates;
	/**
	 * The first state the automata finds itself upon simulation start.
	 */
	private String startingState;
	/**
	 * The first symbol on the stack upon simulation start.
	 */
	private String startingSymbol;
	/**
	 * A map containing transitions which are defined for this particular automata.
	 */
	private Map<Triple,Tuple> transitions;
 	/**
 	 * The stack which the automata needs to function.
 	 */
	private Deque<String> stack;
	
	
	/**
	 * Constructs an automata simulator.
	 * Internally, it initializes all of the attributes our automata needs.
	 */
	public SimPa() {
		listOfInputs = new ArrayList<>();
		allStates = new TreeSet<>();
		allSymbols = new TreeSet<>();
		allStackSymbols = new TreeSet<>();
		acceptedStates = new TreeSet<>();
		stack = new ArrayDeque<String>();
		transitions = new HashMap<>();
	}
	/**
	 * Inner class used to help in determining transitions for the automata.
	 * It has two attributes which describe the next state of our automata 
	 * and the symbols on the stack in the next step.
	 * @author sime
	 *
	 */
	private class Tuple{
		private String nextState;
		private String stackState;
		
		/**
		 * Constructs a tuple containing two strings.
		 * Takes two arguments of type String.
		 * @param nextState
		 * @param stackState
		 */
		public Tuple(String nextState, String stackState) {
			this.nextState = nextState;
			this.stackState = stackState;
		}
		/**
		 * Constructs a tuple containing two strings using an array of strings.
		 * Expected size of array is 2.
		 * Takes an array of Strings as argument.
		 * @param array
		 */
		public Tuple(String[] array) {
			String[] parts = array[1].split(",");
			this.nextState = parts[0];
			this.stackState = parts[1];
		}
		/**
		 * Returns the nextState attribute.
		 * @return nextState;
		 */
		public String getNextState() {
			return nextState;
		}
		/**
		 * Returns the stackState attribute.
		 * @return stackState
		 */
		public String getStackState() {
			return stackState;
		}
		@Override
		public int hashCode() {
			return this.nextState.hashCode() * this.stackState.hashCode();
		}
		@Override
		public boolean equals(Object ob) {
			Tuple other = (Tuple)ob;
			return (other.getNextState().equals(this.getNextState()) && other.getStackState().equals(this.getStackState()));
		}
		
	}
	/**
	 * A helper class used to store triplets of Strings.
	 * It is used to determine transitions in the simulation.
	 * Contains getters for all attributes.
	 * @author Simun Matesic
	 *
	 */
	private class Triple{
		private String currentState;
		private String inputSymbol;
		private String stackSymbol;
		/**
		 * Constructs a Triple of strings.
		 * Takes three arguments of type String.
		 * @param curState
		 * @param inSym
		 * @param stSym
		 */
		public Triple(String curState,String inSym, String stSym) {
			this.currentState = curState;
			this.inputSymbol = inSym;
			this.stackSymbol = stSym;
		}
		/**
		 * Constructs a triple of strings using an array.
		 * The expected size of the array is 3.
		 * Takes an array of Strings as argument.
		 * @param array
		 */
		public Triple(String[] array) {
			String[] parts = array[0].split(",");
			this.currentState = parts[0];
			this.inputSymbol = parts[1];
			this.stackSymbol = parts[2];
		}
		public void setCurrentState(String currentState) {
			this.currentState = currentState;
		}
		public void setInputSymbol(String inputSymbol) {
			this.inputSymbol = inputSymbol;
		}
		public void setStackSymbol(String stackSymbol) {
			this.stackSymbol = stackSymbol;
		}
		/**
		 * Returns the currentState attribute of Triple object.
		 * @return currentState
		 */
		public String getCurrentState() {
			return currentState;
		}
		/**
		 * Returns the inputSymbol attribute of Triple object.
		 * @return inputSymbol
		 */
		public String getInputSymbol() {
			return inputSymbol;
		}
		/**
		 * Returns the stackSymbol attribute of Triple object.
		 * @return stackSymbol
		 */
		public String getStackSymbol() {
			return stackSymbol;
		}			
		@Override
		public int hashCode() {
			return this.currentState.hashCode() * this.inputSymbol.hashCode() *
					this.stackSymbol.hashCode();
		}
		@Override
		public boolean equals(Object ob) {
			Triple other = (Triple) ob;
			return other.getCurrentState().equals(this.getCurrentState()) && 
					other.getInputSymbol().equals(this.getInputSymbol()) &&
						other.getStackSymbol().equals(this.getStackSymbol());
		}
		@Override
		public String toString() {
			return this.getCurrentState() + "," +  this.getInputSymbol() + "," + this.getStackSymbol();
		}
	}
	/**
	 * Extracts data from standard input stream using a BufferedReader object.
	 * Data is stored in the correctregex attributes of automata simulator object.
	 */
	public void extractData() {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));	
		try {
			//Splits the first string by '|' and saves inputs.
			listOfInputs.addAll(Arrays.asList(reader.readLine().split("\\|")));
			//Splits the second string by ',' and saves the states.
			allStates.addAll(Arrays.asList(reader.readLine().split(",")));
			//Splits the third string by ',' and saves the symbols.
			allSymbols.addAll(Arrays.asList(reader.readLine().split(",")));
			//Splits the fourth string by ',' and saves the stack symbols.
			allStackSymbols.addAll(Arrays.asList(reader.readLine().split(",")));
			//Splits the fifth string by ',' and saves accepted states.
			acceptedStates.addAll(Arrays.asList(reader.readLine().split(",")));
			
			//initializes the values.
			startingState = reader.readLine();
			startingSymbol = reader.readLine();
			stack.push(startingSymbol);
		
			String temp;
			while((temp = reader.readLine()) != null){
				//This gets the two sides of a transition in a array of size 2.
				String[] transition = temp.split("->");
				Triple leftSide = new Triple(transition);
				Tuple rightSide = new Tuple(transition);
				//Adds a new transition according to extracted data.
				transitions.put(leftSide, rightSide);
			}
			
		} catch(IOException e) {
			System.err.println("Extraction of data not successful.");
		}
	}
	/**
	 * Simulates the automata.
	 * Prints the results of simulation to standard output stream.
	 * Uses a StringBuilder object to accomplish printing task.
	 * Calls extractData method upon method start.
	 */
	public void simulate() {
		boolean failed = false;
		extractData();
		StringBuilder sb = new StringBuilder();
		Iterator<String> inputChainIterator = listOfInputs.iterator();
		/**
		 * This iterates over all of the input chains which were separated by '|' in the automata definition.
		 */
		while(inputChainIterator.hasNext()) {
			currentState = startingState; //Makes sure the starting position is correct for each input chain.
			//appending the starting state and stack symbol
			sb.append(currentState + "#" + stack.peek() + "|"); //stack.peek() works only because there is only one symbol on the stack.
			
			String inputChain = inputChainIterator.next();
			List<String> symList = Arrays.asList(inputChain.split(",")); //This gives us a list of symbols which are contained in this particular chain.
			
			Iterator<String> symListIterator = symList.iterator();
			/**
			 * This iterates over all of the symbols in a given chain.
			 * DOES NOT INCLUDE "$" SYMBOL! 
			 * This means that when the iterator has reached the end, there are no more symbols to be read.
			 * A check should be made after the iterator to make sure that no epsilon transitions exist and should be executed.
			 */
			while(symListIterator.hasNext()) {
				String symbol = symListIterator.next(); //This is the input symbol.
				/**
				 * At the start of each transition with a given symbol, a check is needed to make sure no epsilon transitions occur.
				 * If there are epsilon transitions, they should be executed.
				 */
				while(true) {
					Triple epsilonTestTr = new Triple(currentState,"$",stack.peek());
					if(transitions.containsKey(epsilonTestTr)) {
						Tuple next = transitions.get(epsilonTestTr);
						epsilonTestTr.setCurrentState(next.getNextState());
						/*
						 * Since the transition gives a string of possibly several states, we need to split them into particular Strings.
						 * This is due to the fact that I use a deque.
						 */
						String[] stackSyms = next.getStackState().split("");
						updateStack(stackSyms);
						currentState = next.getNextState(); //The current state is updated.
						/**
						 * This part append the new states to the stringbuilder.
						 */
						sb.append(currentState + "#");
						stack.forEach(elem->{
							sb.append(elem);
						});
						sb.append("|");
						
					}else { //If there are no existing transitions, the loop is broken.
						break;
					}
				}
				Triple prevState = new Triple(currentState,symbol,stack.peek()); //Makes a triple.
				/**
				 * If the transition does not contain the triple, the simulation is brought to an end with accept constant 0.
				 */
				if(!transitions.containsKey(prevState)) {
					sb.append("fail|0");
					failed = true;
					break;
				}
				/**
				 * The next part is the regular simulation.
				 * A triple is made and a transition is executed.
				 * The stack and currentState are updated.
				 */
				Tuple nextPair = transitions.get(prevState);
				currentState = nextPair.getNextState();
				String[] stackSyms = nextPair.getStackState().split("");
				/**
				 * This is already written in the loop used to find epsilon productions.
				 * The code will be extracted to a new method.
				 */
				updateStack(stackSyms);
				/**
				 * Appending to stringbuilder
				 */
				sb.append(currentState + "#");
				stack.forEach(el->{
					sb.append(el);
				});
				sb.append("|");
			}
			if(!failed) {
				/**
				 * Now an important part of the code.
				 * The input string is all read, but - if there are epsilon transitions from the current state they should be executed, but only
				 * if the currentState is not accepted.
				 */
				Triple epsilonTriple = new Triple(currentState, "$", stack.peek());
				while(true) {	
					if(!acceptedStates.contains(currentState)) {
						if(transitions.containsKey(epsilonTriple)) {
							Tuple result = transitions.get(epsilonTriple);			
							currentState = result.getNextState();
							epsilonTriple.setCurrentState(result.getNextState());
							String[] stackSyms = result.getStackState().split("");
							
							updateStack(stackSyms);
							epsilonTriple.setStackSymbol(stack.peek());
							
							sb.append(currentState + "#");
							stack.forEach(el->{
								sb.append(el);
							});
							sb.append("|");
						}
						else {
							break;
						}
					}
					else {
						break;
					}
					
				}
				/**
				 * Now that we are sure about the epsilon transitions, we can determine whether the state is accepted or not.
				 */
				if(acceptedStates.contains(currentState)) {
					sb.append("1");
				}
				else {
					sb.append("0");
				}
				
			}
			if(inputChainIterator.hasNext()) {
				sb.append("\n");
			}
			failed = false;
			stack.clear();
			stack.push(startingSymbol);
			currentState = startingState;
		}
		System.out.println(sb.toString());
	}
	public void updateStack(String[] stackSyms) {
		stack.pop();
		for(int i = stackSyms.length-1; i >= 0; i--) {
			if(stackSyms[0].equals("$")) {
				if(stack.isEmpty()) {
					stack.push("$");
				}
				break;
			}
			stack.push(stackSyms[i]);
		}
	}
	/**
	 * Main method used to simulate PD automata.
	 * @param args
	 */
	public static void main(String[] args) {
		SimPa simulator = new SimPa();
		simulator.simulate();
	}
	
}
